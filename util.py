# 引入必要的库
# TODO

# 获取 0_setting.yaml 中的键 key 对应的值 value
def get(key):
    # TODO
    return value

# 预处理图像, 把图像设置为指定大小之后，展平返回
def preprocess_image(file_name, new_size):
    # 1. 读取图像灰度图
    # TODO
    # 2. 调整图像大小为 new_size
    # TODO
    # 3. 将图像展平为一维数组
    # TODO
    return img

# 用joblib把叫做 name 的对象 obj 保存(序列化)到位置 loc
def dump(obj, name, loc):
    start = time.time()
    print(f"把{name}保存到{loc}") 
    # TODO 此处序列化对象
    end = time.time()
    print(f"保存完毕,文件位置:{loc}, 大小:{os.path.getsize(loc) / 1024 / 1024:.3f}M")
    print(f"运行时间:{end - start:.3f}秒")

# 用joblib读取(反序列化)位置loc的对象obj,对象名为name
def load(name, loc):
    print(f"从{loc}提取文件{name}")
    #TODO 此处反序列化对象
    return obj